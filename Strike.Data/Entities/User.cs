﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Strike.Data.Entities
{
    public class User : Entity
    {
        [Required]
        [EmailAddress]
        [MaxLength(254)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        [StringLength(maximumLength: 128, MinimumLength = 8)]
        [RegularExpression(@"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*]).{8,}$")]
        public string Password { get; set; }

        [Required]
        [StringLength(maximumLength: 128, MinimumLength = 2)]
        public string Name { get; set; }
    }
}