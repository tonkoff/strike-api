﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Strike.Data.Entities
{
    public class Applicant : Entity
    {
        [Required]
        [EmailAddress]
        [MaxLength(254)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Phone]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        [Required]
        [StringLength(maximumLength: 128, MinimumLength = 2)]
        public string Name { get; set; }
    }
}
