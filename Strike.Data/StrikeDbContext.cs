﻿using Microsoft.EntityFrameworkCore;
using Strike.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Strike.Data
{
    public class StrikeDbContext : DbContext
    {
        public StrikeDbContext(DbContextOptions<StrikeDbContext> options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }

        public DbSet<Applicant> Applicants { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}