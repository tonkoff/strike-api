﻿using Microsoft.EntityFrameworkCore.Storage;
using Strike.Data.Entities;
using System;
using System.Linq;

namespace Strike.Data.Repositories.Interfaces
{
    public interface IBaseRepository<TEntity>
        where TEntity : Entity
    {
        void Insert(TEntity item);

        void Update(TEntity item);

        void Delete(TEntity item);

        TEntity Get(Guid id);

        TEntity GetAsNoTracking(Guid id);

        IQueryable<TEntity> GetAll();

        IQueryable<TEntity> GetAllAsNoTracking();

        IDbContextTransaction BeginTransaction();

        void CommitTransaction();

        void Save();
    }
}