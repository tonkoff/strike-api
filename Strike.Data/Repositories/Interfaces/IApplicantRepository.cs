﻿using Strike.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Strike.Data.Repositories.Interfaces
{
    public interface IApplicantRepository : IBaseRepository<Applicant>
    {
    }
}
