﻿using Strike.Data.Entities;
using Strike.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Strike.Data.Repositories.Implementations
{
    public class ApplicantRepository : BaseRepository<Applicant>, IApplicantRepository
    {
        public ApplicantRepository(StrikeDbContext dbContext) : base(dbContext)
        {
        }
    }
}
