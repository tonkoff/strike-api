﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Strike.Data.Entities;
using Strike.Data.Repositories.Interfaces;
using System;
using System.Linq;

namespace Strike.Data.Repositories.Implementations
{
    public abstract class BaseRepository<TEntity> : IBaseRepository<TEntity>
        where TEntity : Entity
    {
        protected readonly StrikeDbContext dbContext;

        protected DbSet<TEntity> Items;

        public BaseRepository(StrikeDbContext dbContext)
        {
            this.dbContext = dbContext;
            Items = dbContext.Set<TEntity>();
        }

        public IDbContextTransaction BeginTransaction()
        {
            return dbContext
                .Database
                .BeginTransaction();
        }

        public void CommitTransaction()
        {
            dbContext
                .Database
                .CommitTransaction();
        }

        public virtual void Delete(TEntity item)
        {
            dbContext.Remove(item);
        }

        public virtual TEntity Get(Guid id)
        {
            return Items.SingleOrDefault(item => item.Id == id);
        }

        public virtual IQueryable<TEntity> GetAll()
        {
            return Items;
        }

        public virtual IQueryable<TEntity> GetAllAsNoTracking()
        {
            return Items.AsNoTracking();
        }

        public virtual TEntity GetAsNoTracking(Guid id)
        {
            return Items
                .AsNoTracking()
                .SingleOrDefault(item => item.Id == id);
        }

        public virtual void Insert(TEntity item)
        {
            Items.Add(item);
        }

        public void Save()
        {
            dbContext.SaveChanges();
        }

        public virtual void Update(TEntity item)
        {
            var state = dbContext.Entry(item).State;

            dbContext.Entry(item).State = EntityState.Modified;
        }
    }
}