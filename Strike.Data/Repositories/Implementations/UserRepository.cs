﻿using Strike.Data.Entities;
using Strike.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Strike.Data.Repositories.Implementations
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(StrikeDbContext dbContext) : base(dbContext)
        {
        }
    }
}
