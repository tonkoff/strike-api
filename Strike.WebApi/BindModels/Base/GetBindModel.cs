﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Strike.WebApi.BindModels.Base
{
    public class GetBindModel
    {
        public Guid Id { get; set; }
    }
}
