﻿using Strike.WebApi.BindModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Strike.WebApi.BindModels.Applicant
{
    public class ApplicantGetBindModel : GetBindModel
    {
        public string Email { get; set; }

        public string Phone { get; set; }

        public string Name { get; set; }
    }
}
