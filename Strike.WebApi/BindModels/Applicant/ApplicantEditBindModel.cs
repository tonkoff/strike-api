﻿using Strike.WebApi.BindModels.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Strike.WebApi.BindModels.Applicant
{
    public class ApplicantEditBindModel : EditBindModel
    {
        [Required]
        [EmailAddress]
        [MaxLength(254)]
        public string Email { get; set; }

        [Phone]
        public string Phone { get; set; }

        [Required]
        [StringLength(maximumLength: 128, MinimumLength = 2)]
        public string Name { get; set; }
    }
}
