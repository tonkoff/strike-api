﻿using Strike.WebApi.BindModels.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Strike.WebApi.BindModels.User
{
    public class UserEditBindModel : EditBindModel
    {
        [Required]
        [EmailAddress]
        [MaxLength(254)]
        public string Email { get; set; }

        [StringLength(maximumLength: 128, MinimumLength = 8)]
        [RegularExpression(@"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*]).{8,}$")]
        public string Password { get; set; }

        [Required]
        [StringLength(maximumLength: 128, MinimumLength = 2)]
        public string Name { get; set; }
    }
}
