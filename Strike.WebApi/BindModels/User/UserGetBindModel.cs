﻿using Strike.WebApi.BindModels.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Strike.WebApi.BindModels.User
{
    public class UserGetBindModel : GetBindModel
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public string Name { get; set; }
    }
}