﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Strike.WebApi.BindModels.User
{
    public class LoginBindModel
    {
        [Required]
        [EmailAddress]
        [MaxLength(254)]
        public string Email { get; set; }

        [RegularExpression(@"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*]).{8,}$")]
        [MaxLength(128)]
        public string Password { get; set; }
    }
}
