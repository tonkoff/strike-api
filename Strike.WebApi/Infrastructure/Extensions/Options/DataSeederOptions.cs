﻿using Microsoft.EntityFrameworkCore;
using Strike.Data;
using Strike.Data.Entities;
using System.Linq;

namespace Strike.WebApi.Infrastructure.Extensions.Options
{
    public class DataSeederOptions
    {
        public readonly StrikeDbContext dbContext;

        public DataSeederOptions(StrikeDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public DataSeederOptions SeedAdmin()
        {
            const string AdminEmail = "admin@admin.com";

            if (!dbContext.Users.Any(x => x.Email == AdminEmail))
            {
                dbContext.Users.Add(new User
                {
                    Email = AdminEmail,
                    Password = "Str1k3?!",
                    Name = "Administrator"
                });
            }

            return this;
        }

        public DataSeederOptions Migrate()
        {
            dbContext.Database.Migrate();

            return this;
        }
    }
}
