﻿using Microsoft.Extensions.DependencyInjection;
using Strike.Data;
using Strike.WebApi.Infrastructure.Extensions.Options;
using System;

namespace Strike.WebApi.Infrastructure.Extensions
{
    public static class ServiceCollectionExtenstions
    {
        public static IServiceCollection SeedDatabase(this IServiceCollection services, Action<DataSeederOptions> options)
        {
            var dbContext = services.BuildServiceProvider().GetService<StrikeDbContext>();

            using (dbContext)
            {
                options.Invoke(new DataSeederOptions(dbContext));

                dbContext.SaveChanges();
            }

            return services;
        }
    }
}
