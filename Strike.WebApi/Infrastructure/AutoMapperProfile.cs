﻿using AutoMapper;
using Strike.Data.Entities;
using Strike.Services.DomainModels;
using Strike.WebApi.BindModels.Applicant;
using Strike.WebApi.BindModels.User;

namespace Strike.WebApi.Infrastructure
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserDomainModel>().ReverseMap();
            CreateMap<UserDomainModel, LoginBindModel>().ReverseMap();
            CreateMap<UserDomainModel, UserGetBindModel>().ReverseMap();
            CreateMap<UserDomainModel, UserEditBindModel>().ReverseMap();

            CreateMap<Applicant, ApplicantDomainModel>().ReverseMap();
            CreateMap<ApplicantDomainModel, ApplicantGetBindModel>().ReverseMap();
            CreateMap<ApplicantDomainModel, ApplicantEditBindModel>().ReverseMap();
        }
    }
}