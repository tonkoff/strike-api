﻿using System.Buffers;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Strike.Data;
using Strike.Data.Repositories.Implementations;
using Strike.Data.Repositories.Interfaces;
using Strike.Services.Implementations.Auth;
using Strike.Services.Implementations.Data;
using Strike.Services.Implementations.Email;
using Strike.Services.Implementations.EmailConfiguration;
using Strike.Services.Interfaces.Auth;
using Strike.Services.Interfaces.Data;
using Strike.Services.Interfaces.Email;
using Strike.WebApi.Infrastructure.Extensions;
using Swashbuckle.AspNetCore.Swagger;

namespace Strike.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options =>
            {
                options.OutputFormatters.Clear();
                options.OutputFormatters.Add(new JsonOutputFormatter(new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                }, ArrayPool<char>.Shared));
            });

            services.AddDbContext<StrikeDbContext>(options =>
                options.UseMySql(Configuration.GetConnectionString("DefaultConnection")));

            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IAuthService, AuthService>();
            services.AddScoped<IEmailParserService, EmailParserService>();
            services.AddScoped<IApplicantRepository, ApplicantRepository>();
            services.AddScoped<IApplicantService, ApplicantService>();

            services.AddSingleton<IEmailSenderService, EmailSenderService>();

            var emailConfig = new EmailConfig();
            Configuration.Bind("EmailSettings", emailConfig);
            services.AddSingleton(emailConfig);

            services.SeedDatabase(options =>
            {
                options.Migrate().SeedAdmin();
            });

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new Info
                {
                    Title = "Strike Web API",
                    Version = "v1"
                });
            });

            services.AddCors();

            services.AddAutoMapper();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder =>
                builder.WithOrigins("https://localhost:4200")
                .AllowAnyHeader());

            app.UseSwagger();

            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "Strike Web API v1");
            });

            app.UseMvc();
        }
    }
}