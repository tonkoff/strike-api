﻿using Microsoft.AspNetCore.Mvc;
using Strike.Data.Entities;
using Strike.Services.DomainModels;
using Strike.Services.Interfaces.Data;
using Strike.WebApi.BindModels;
using Strike.WebApi.BindModels.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Strike.WebApi.Controllers
{
    [Route("api/users")]
    public class UsersController : CrudApiController<
        IUserService, 
        UserDomainModel, 
        UserGetBindModel, 
        UserEditBindModel>
    {
        public UsersController(IUserService service) : base(service)
        {
        }
    }
}