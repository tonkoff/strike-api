﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Strike.Services.DomainModels;
using Strike.Services.Interfaces;
using Strike.Services.Interfaces.Data;
using Strike.WebApi.BindModels;
using Strike.WebApi.BindModels.Base;

namespace Strike.WebApi.Controllers
{
    [Produces("application/json")]
    public abstract class CrudApiController<
        TService, 
        TDomainModel, 
        TGetBindModel, 
        TEditBindModel> : BaseController
        where TService: IDataService<TDomainModel>
        where TDomainModel : DomainModel
        where TGetBindModel : GetBindModel
        where TEditBindModel : EditBindModel
    {
        protected readonly TService service;

        public CrudApiController(TService service)
        {
            this.service = service;
        }

        [HttpGet]
        public virtual IActionResult Get()
        {
            var items = AutoMapper
                            .Mapper
                            .Map<IEnumerable<TDomainModel>, 
                            IEnumerable<TGetBindModel>>(service.GetAll());

            return Ok(items);
        }

        [HttpGet("{id}")]
        public virtual IActionResult Get(Guid id)
        {
            var item = AutoMapper
                        .Mapper
                        .Map<TDomainModel, 
                        TGetBindModel>(service.Get(id));

            if (item == null)
            {
                return NotFound();
            }

            return Ok(item);
        }

        [HttpPost]
        public virtual IActionResult Post([FromBody]TEditBindModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(GetModelStateErrors(ModelState));
            }

            var item = AutoMapper
                        .Mapper
                        .Map<TEditBindModel, 
                        TDomainModel>(model);

            try
            {
                service.Insert(item);
                service.Save();
            }
            catch (Exception e)
            {
                return BadRequest(GetExceptionMessege(e));
            }

            return CreatedAtRoute(new { id = item.Id }, item);
        }

        [HttpPut("{id}")]
        public virtual IActionResult Put(Guid id, [FromBody]TEditBindModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(GetModelStateErrors(ModelState));
            }

            if (service.GetAsNoTracking(id) == null)
            {
                return NotFound();
            }

            var item = AutoMapper
                .Mapper
                .Map<TEditBindModel, TDomainModel>(model);

            item.Id = id;

            try
            {
                service.Update(item);
                service.Save();
            }
            catch (Exception e)
            {
                return BadRequest(GetExceptionMessege(e));
            }

            return Ok(item);
        }

        [HttpPatch("{id}")]
        public virtual IActionResult Patch(Guid id, 
            [FromBody]JsonPatchDocument<TEditBindModel> patchDoc)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(GetModelStateErrors(ModelState));
            }

            var item = AutoMapper
                        .Mapper
                        .Map<TDomainModel, 
                        TEditBindModel>(service.GetAsNoTracking(id));

            if (item == null)
            {
                return NotFound();
            }

            try
            {
                patchDoc.ApplyTo(item);

                var domainModel = AutoMapper
                                .Mapper
                                .Map<TEditBindModel, TDomainModel>(item);
                domainModel.Id = id;

                service.Update(domainModel);
                service.Save();
            }
            catch (Exception e)
            {
                return BadRequest(GetExceptionMessege(e));
            }

            return Ok(item);
        }

        [HttpDelete("{id}")]
        public virtual IActionResult Delete(Guid id)
        {
            var item = service.GetAsNoTracking(id);

            if (item == null)
            {
                return NotFound();
            }

            try
            {
                service.Delete(item);
                service.Save();
            }
            catch (Exception e)
            {
                return BadRequest(GetExceptionMessege(e));
            }

            return Ok();
        }
    }
}