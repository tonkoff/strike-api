﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Strike.WebApi.Controllers
{
    public abstract class BaseController : Controller
    {
        protected string GetExceptionMessege(Exception exception)
        {
            string message = exception.Message;

            if (exception.InnerException != null)
            {
                message = exception.InnerException.Message;
            }

            return message;
        }

        protected IEnumerable<string> GetModelStateErrors(ModelStateDictionary modelState)
        {
            foreach (var entry in modelState.Values)
            {
                foreach (var error in entry.Errors)
                {
                    yield return error.ErrorMessage;
                }
            }
        }
    }
}