﻿using Microsoft.AspNetCore.Mvc;
using Strike.Services.DomainModels;
using Strike.Services.Interfaces.Auth;
using Strike.Services.Interfaces.Data;
using Strike.WebApi.BindModels;
using Strike.WebApi.BindModels.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Strike.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/auth")]
    public class AuthController : BaseController
    {
        private readonly IAuthService authService;
        private readonly IUserService userService;

        public AuthController(
            IAuthService authService,
            IUserService userService)
        {
            this.authService = authService;
            this.userService = userService;
        }

        [HttpPost]
        public IActionResult Login([FromBody]LoginBindModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(GetModelStateErrors(ModelState));
            }

            try
            {
                if (userService.Get(model.Email, model.Password) == null)
                {
                    return NotFound();
                }

                return Ok(authService.GenerateJwtToken(model.Email, model.Password));
            }
            catch (Exception e)
            {
                return BadRequest(GetExceptionMessege(e));
            }
        }
    }
}