﻿using Microsoft.AspNetCore.Mvc;
using Strike.Services.DomainModels;
using Strike.Services.Interfaces.Data;
using Strike.WebApi.BindModels.Applicant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Strike.WebApi.Controllers
{
    [Route("api/applicants")]
    public class ApplicantsController : CrudApiController<
        IApplicantService,
        ApplicantDomainModel,
        ApplicantGetBindModel,
        ApplicantEditBindModel>
    {
        public ApplicantsController(IApplicantService service) : base(service)
        {
        }
    }
}
