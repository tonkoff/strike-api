﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Strike.Services.Interfaces.Email
{
    public interface IEmailParserService
    {
        string EmailParser<T>(string templeate, T obj);
    }
}