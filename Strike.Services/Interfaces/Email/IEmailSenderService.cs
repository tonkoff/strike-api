﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Strike.Services.Interfaces.Email
{
    public interface IEmailSenderService
    {
        Task SendEmailAsync(string email, string subject, string message, List<string> cc = null, List<string> bcc = null);
    }
}