﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Strike.Services.Interfaces.Auth
{
    public interface IAuthService
    {
        string GenerateJwtToken(string email, string password);
    }
}