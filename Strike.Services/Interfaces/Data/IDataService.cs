﻿using Microsoft.EntityFrameworkCore.Storage;
using Strike.Services.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Strike.Services.Interfaces.Data
{
    public interface IDataService<TDomainModel>
        where TDomainModel : DomainModel
    {
        void Insert(TDomainModel model);

        void Update(TDomainModel model);

        void Delete(TDomainModel model);

        TDomainModel Get(Guid id);

        TDomainModel GetAsNoTracking(Guid id);

        IEnumerable<TDomainModel> GetAll();

        IEnumerable<TDomainModel> GetAll(Expression<Func<TDomainModel, bool>> filter);

        IEnumerable<TDomainModel> GetAll(Expression<Func<TDomainModel, object>> orderBy);

        IEnumerable<TDomainModel> GetAll(Expression<Func<TDomainModel, bool>> filter, Expression<Func<TDomainModel, object>> orderBy);

        IEnumerable<TDomainModel> GetAllAsNoTracking();

        IEnumerable<TDomainModel> GetAllAsNoTracking(Expression<Func<TDomainModel, bool>> filter);

        IEnumerable<TDomainModel> GetAllAsNoTracking(Expression<Func<TDomainModel, object>> orderBy);

        IEnumerable<TDomainModel> GetAllAsNoTracking(Expression<Func<TDomainModel, bool>> filter, Expression<Func<TDomainModel, object>> orderBy);

        bool IsValid(TDomainModel model);

        IDbContextTransaction BeginTransaction();

        void CommitTransaction();

        void Save();
    }
}