﻿using Strike.Services.DomainModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Strike.Services.Interfaces.Data
{
    public interface IApplicantService : IDataService<ApplicantDomainModel>
    {
    }
}
