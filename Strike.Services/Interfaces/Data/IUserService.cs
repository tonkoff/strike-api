﻿using Strike.Services.DomainModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Strike.Services.Interfaces.Data
{
    public interface IUserService : IDataService<UserDomainModel>
    {
        UserDomainModel Get(string email, string password);
    }
}