﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Strike.Services.Interfaces.Auth;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

namespace Strike.Services.Implementations.Auth
{
    public class AuthService : IAuthService
    {
        private readonly IConfiguration configuration;

        public AuthService(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public string GenerateJwtToken(string email, string password)
        {
            SymmetricSecurityKey securityKey = new SymmetricSecurityKey(
                Encoding
                .UTF8
                .GetBytes(configuration["Keys:Secret"]));

            SigningCredentials credentials = new SigningCredentials(securityKey, 
                SecurityAlgorithms.HmacSha256Signature);

            JwtHeader header = new JwtHeader(credentials);
            JwtPayload payload = new JwtPayload
            {
                { "Email", email },
                { "Password", password }
            };

            JwtSecurityToken securityToken = new JwtSecurityToken(header, payload);

            return new JwtSecurityTokenHandler().WriteToken(securityToken);
        }
    }
}