﻿using Strike.Data.Entities;
using Strike.Data.Repositories.Interfaces;
using Strike.Services.DomainModels;
using Strike.Services.Interfaces.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Strike.Services.Implementations.Data
{
    public class UserService : DataService<User, UserDomainModel>, IUserService
    {
        public UserService(IUserRepository repository) : base(repository)
        {
        }

        public UserDomainModel Get(string email, string password)
        {
            var user = AutoMapper
                .Mapper
                .Map<User, UserDomainModel>(
                repository
                .GetAll()
                .SingleOrDefault(x => x.Email == email 
                && x.Password == password));

            return user;
        }
    }
}
