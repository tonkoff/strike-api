﻿using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore.Storage;
using Strike.Data.Entities;
using Strike.Data.Repositories.Interfaces;
using Strike.Services.DomainModels;
using Strike.Services.Interfaces.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Strike.Services.Implementations.Data
{
    public abstract class DataService<TEntity, TDomainModel> : IDataService<TDomainModel>
        where TEntity : Entity
        where TDomainModel : DomainModel
    {
        protected readonly IBaseRepository<TEntity> repository;

        public DataService(IBaseRepository<TEntity> repository)
        {
            this.repository = repository;
        }

        public virtual TDomainModel Get(Guid id)
        {
            return AutoMapper
                .Mapper
                .Map<TEntity, TDomainModel>(repository.Get(id));
        }

        public virtual TDomainModel GetAsNoTracking(Guid id)
        {
            return AutoMapper
                .Mapper
                .Map<TEntity, TDomainModel>(repository
                .GetAsNoTracking(id));
        }

        public virtual IEnumerable<TDomainModel> GetAll()
        {
            return repository
                .GetAll()
                .ProjectTo<TDomainModel>();
        }

        public virtual IEnumerable<TDomainModel> GetAll(
            Expression<Func<TDomainModel, bool>> filter)
        {
            return repository
                .GetAll()
                .ProjectTo<TDomainModel>()
                .Where(filter);
        }

        public virtual IEnumerable<TDomainModel> GetAll(
            Expression<Func<TDomainModel, object>> orderBy)
        {
            return repository
                .GetAll()
                .ProjectTo<TDomainModel>()
                .OrderBy(orderBy);
        }

        public virtual IEnumerable<TDomainModel> GetAll(
            Expression<Func<TDomainModel, bool>> filter,
            Expression<Func<TDomainModel, object>> orderBy)
        {
            return repository
                .GetAll()
                .ProjectTo<TDomainModel>()
                .Where(filter)
                .OrderBy(orderBy);
        }

        public virtual IEnumerable<TDomainModel> GetAllAsNoTracking()
        {
            return repository
                .GetAllAsNoTracking()
                .ProjectTo<TDomainModel>();
        }

        public virtual IEnumerable<TDomainModel> GetAllAsNoTracking(
            Expression<Func<TDomainModel, bool>> filter)
        {
            return repository
                .GetAllAsNoTracking()
                .ProjectTo<TDomainModel>()
                .Where(filter);
        }

        public virtual IEnumerable<TDomainModel> GetAllAsNoTracking(
            Expression<Func<TDomainModel, object>> orderBy)
        {
            return repository
                .GetAllAsNoTracking()
                .ProjectTo<TDomainModel>()
                .OrderBy(orderBy);
        }

        public virtual IEnumerable<TDomainModel> GetAllAsNoTracking(
            Expression<Func<TDomainModel, bool>> filter,
            Expression<Func<TDomainModel, object>> orderBy)
        {
            return repository
                .GetAllAsNoTracking()
                .ProjectTo<TDomainModel>()
                .Where(filter)
                .OrderBy(orderBy);
        }

        public virtual void Delete(TDomainModel model)
        {
            OnBeforeDelete(model);

            var item = AutoMapper.Mapper.Map<TDomainModel, TEntity>(model);
            repository.Delete(item);

            OnAfterDelete(model);
        }

        public virtual void Insert(TDomainModel model)
        {
            OnBeforeInsert(model);

            if (!IsValid(model))
            {
                throw new Exception("Model is invalid!");
            }

            var item = AutoMapper.Mapper.Map<TDomainModel, TEntity>(model);
            repository.Insert(item);

            OnAfterInsert(model);
        }

        public virtual void Update(TDomainModel model)
        {
            OnBeforeUpdate(model);

            if (!IsValid(model))
            {
                throw new Exception("Model is invalid!");
            }

            var item = AutoMapper.Mapper.Map<TDomainModel, TEntity>(model);
            repository.Update(item);

            OnAfterUpdate(model);
        }

        public IDbContextTransaction BeginTransaction()
        {
            return repository.BeginTransaction();
        }

        public void CommitTransaction()
        {
            repository.CommitTransaction();
        }

        public void Save()
        {
            repository.Save();
        }

        public virtual bool IsValid(TDomainModel model) => true;

        protected virtual void OnBeforeInsert(TDomainModel model) { }

        protected virtual void OnAfterInsert(TDomainModel model) { }

        protected virtual void OnBeforeUpdate(TDomainModel model) { }

        protected virtual void OnAfterUpdate(TDomainModel model) { }

        protected virtual void OnBeforeDelete(TDomainModel model) { }

        protected virtual void OnAfterDelete(TDomainModel model) { }
    }
}