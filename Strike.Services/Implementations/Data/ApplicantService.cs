﻿using Strike.Data.Entities;
using Strike.Data.Repositories.Interfaces;
using Strike.Services.DomainModels;
using Strike.Services.Interfaces.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Strike.Services.Implementations.Data
{
    public class ApplicantService : DataService<Applicant, ApplicantDomainModel>, IApplicantService
    {
        public ApplicantService(IApplicantRepository repository) : base(repository)
        {
        }
    }
}
