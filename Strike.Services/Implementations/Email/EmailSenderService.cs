﻿using Strike.Services.Interfaces.Email;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using MimeKit;
using Strike.Services.Implementations.EmailConfiguration;

namespace Strike.Services.Implementations.Email
{
    public class EmailSenderService : IEmailSenderService
    {
        private EmailConfig conf;

        public EmailSenderService(EmailConfig configuration)
        {
            conf = configuration;
        }

        public async Task SendEmailAsync(string email, string subject, string message, List<string> cc = null, List<string> bcc = null)
        {
            try
            {
                string FromAddress = conf.From;
                string FromAdressTitle = conf.FromAddressTitle;

                var mimeMessage = new MimeMessage();

                mimeMessage.From.Add(new MailboxAddress(FromAdressTitle, FromAddress));
                mimeMessage.To.Add(new MailboxAddress(email, email));

                mimeMessage.Subject = subject;
                mimeMessage.Body = new TextPart("html")
                {
                    Text = message
                };

                if (cc != null)
                    foreach (var ccAdd in cc)
                    {
                        MailboxAddress ccmail = new MailboxAddress(ccAdd);

                        mimeMessage.Cc.Add(ccmail);
                    }
                if (bcc != null)
                    foreach (var bccAdd in bcc)
                    {
                        MailboxAddress bbmail = new MailboxAddress(bccAdd);

                        mimeMessage.Bcc.Add(bbmail);
                    }

                //Mail Trap server

                using (var client = new SmtpClient())
                {
                    client.Connect(conf.EmailServer, conf.EmailServerPort, false);
                    client.Authenticate(
                        conf.EmailUsername,
                        conf.EmailPassword
                        );
                    await client.SendAsync(mimeMessage);

                    await client.DisconnectAsync(true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}