﻿using Strike.Services.Interfaces.Email;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Strike.Services.Implementations.Email
{
    public class EmailParserService : IEmailParserService
    {
        public string EmailParser<T>(string template, T obj)
        {
            try
            {
                string pattern = @"{{?(.*)}}";

                MatchCollection placeHolders = Regex.Matches(template, pattern);

                foreach (Match placeHolder in placeHolders)
                {
                    string placeHolderName = placeHolder.Groups[1].Value;
                    var result = typeof(T).GetProperties().FirstOrDefault(x => x.Name == placeHolderName);

                    if (result != null)
                    {
                        var value = obj.GetType().GetProperty(result.Name).GetValue(obj);

                        if (value != null)
                        {
                            template = Regex.Replace(template, placeHolder.ToString(), value.ToString());
                        }
                        else
                        {
                            throw new NullReferenceException();
                        }
                    }
                    else
                    {
                        throw new ArgumentNullException();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return template;
        }
    }
}