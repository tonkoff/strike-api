﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Strike.Services.Implementations.EmailConfiguration
{
    public class EmailConfig
    {
        public string From { get; set; }
        public string FromAddressTitle { get; set; }
        public string EmailUsername { get; set; }
        public string EmailPassword { get; set; }
        public string EmailServer { get; set; }
        public int EmailServerPort { get; set; }
    }
}