﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Strike.Services.DomainModels
{
    public class ApplicantDomainModel : DomainModel
    {
        public string Email { get; set; }

        public string Phone { get; set; }

        public string Name { get; set; }
    }
}
