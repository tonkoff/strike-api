﻿using FluentAssertions;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Strike.Data;
using Strike.Data.Repositories.Implementations;
using Strike.Services.DomainModels;
using Strike.Services.Implementations.Data;
using Strike.Services.Interfaces.Data;
using Strike.WebApi.BindModels.Applicant;
using Strike.WebApi.Controllers;
using Strike.WebApi.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace Strike.Tests.WebApi.Controllers
{
    [TestClass]
    public class ApplicantsControllerTest
    {
        private static IApplicantService applicantService;
        private ApplicantsController applicantsController;
        private const string Id = "7682df84-6322-4f11-abab-ce320ecc1a3a";

        [ClassInitialize]
        public static void ClassInitialize(TestContext test)
        {
            AutoMapper.Mapper.Initialize(x => x.AddProfile<AutoMapperProfile>());

            var options = new DbContextOptionsBuilder<StrikeDbContext>()
                .UseInMemoryDatabase("StrikeDb")
                .Options;

            applicantService = new ApplicantService(
                new ApplicantRepository(
                    new StrikeDbContext(options)));

            applicantService.Insert(new ApplicantDomainModel
            {
                Email = "test@test.com",
                Name = "Test",
                Phone = "081293823",
                Id = Guid.Parse(Id)
            });
            applicantService.Save();
        }

        [TestInitialize]
        public void TestInitialize()
        {
            //arrange
            this.applicantsController = new ApplicantsController(applicantService);
        }

        [TestMethod]
        public void Get_ItemNotFound_ReturnsNotFoundResult()
        {
            //arrange

            //act
            var actionResult = applicantsController.Get(default(Guid));

            //assert
            actionResult.Should().BeAssignableTo<NotFoundResult>();
        }

        [TestMethod]
        public void Get_ItemFound_ReturnsOkResultWithObject()
        {
            //arrange

            //act
            var actionResult = applicantsController.Get(Guid.Parse(Id));

            //assert
            actionResult.Should().BeAssignableTo<OkObjectResult>();
        }

        [TestMethod]
        public void Get_ReturnsOkResultWithObject()
        {
            //arrange

            //act
            var actionResult = applicantsController.Get();

            //assert
            actionResult.Should().BeAssignableTo<OkObjectResult>();
        }

        [TestMethod]
        public void Post_ModelStateIsNotValid_ReturnsBadRequestResultWithObject()
        {
            //arrange
            applicantsController
                .ModelState
                .AddModelError("Test Error", "This is a test error");

            //act
            var actionResult = applicantsController.Post(null);

            //assert
            applicantsController
                .ModelState
                .IsValid
                .Should()
                .BeFalse();

            actionResult
                .Should()
                .BeAssignableTo<BadRequestObjectResult>();
        }

        [TestMethod]
        public void Post_ModelIsValid_ReturnsCreatedAtRouteResult()
        {
            //arrange
            var item = new ApplicantEditBindModel
            {
                Email = "test@test.com",
                Name = "Test",
                Phone = "0038459"
            };

            // act
            var actionResult = applicantsController.Post(item);

            //assert
            applicantsController
                .ModelState
                .IsValid
                .Should()
                .BeTrue();

            actionResult
                .Should()
                .BeAssignableTo<CreatedAtRouteResult>();
        }

        [TestMethod]
        public void Put_ModelStateIsNotValid_ReturnsBadRequestResultWithObject()
        {
            //arrange
            applicantsController
                .ModelState
                .AddModelError("Test Error", "This is a test error");

            //act
            var actionResult = applicantsController.Put(default(Guid), null);

            //assert
            applicantsController
                .ModelState
                .IsValid
                .Should()
                .BeFalse();

            actionResult
                .Should()
                .BeAssignableTo<BadRequestObjectResult>();
        }

        [TestMethod]
        public void Put_ItemNotFound_ReturnsNotFoundResult()
        {
            //arrange

            //act
            var actionResult = applicantsController.Put(default(Guid), null);

            //assert
            applicantsController
                .ModelState
                .IsValid
                .Should()
                .BeTrue();

            actionResult
                .Should()
                .BeAssignableTo<NotFoundResult>();
        }

        [TestMethod]
        [Ignore]
        public void Put_ModelFoundAndValid_ReturnsOkResultWithObject()
        {
            //arrange
            var item = new ApplicantEditBindModel
            {
                Email = "updated@updated.com",
                Name = "updated",
                Phone = "0038459"
            };

            //act
            var actionResult = applicantsController.Put(Guid.Parse(Id), item);

            //assert
            applicantsController
               .ModelState
               .IsValid
               .Should()
               .BeTrue();

            actionResult
                .Should()
                .BeAssignableTo<OkObjectResult>();
        }

        [TestMethod]
        public void Delete_ItemNotFound_ReturnsNotFoundResult()
        {
            //arrange

            //act
            var actionResult = applicantsController.Delete(default(Guid));

            //assert
            actionResult
                .Should()
                .BeAssignableTo<NotFoundResult>();
        }

        [TestMethod]
        [Ignore]
        public void Delete_ItemFound_ReturnsOkResultWithObject()
        {
            //arrange

            //act
            var actionResult = applicantsController.Delete(Guid.Parse(Id));

            //assert
            actionResult
                .Should()
                .BeAssignableTo<OkObjectResult>();
        }

        [TestMethod]
        public void Patch_ItemNotFound_ReturnsNotFoundResult()
        {
            //arrange

            //act
            var actionResult = applicantsController.Patch(default(Guid), null);

            //assert
            applicantsController
                .ModelState
                .IsValid
                .Should()
                .BeTrue();

            actionResult
                .Should()
                .BeAssignableTo<NotFoundResult>();
        }

        [TestMethod]
        public void Patch_ModelStateIsNotValid_ReturnsBadRequestResultWithObject()
        {
            //arrange

            //act
            var actionResult = applicantsController.Patch(Guid.Parse(Id), null);
            applicantsController
                .ModelState
                .AddModelError("Test Error", "This is a test error");

            //assert
            applicantsController
                .ModelState
                .IsValid
                .Should()
                .BeFalse();

            actionResult
                .Should()
                .BeAssignableTo<BadRequestObjectResult>();
        }

        [TestMethod]
        [Ignore]
        public void Patch_ItemIsFoundAndModelStateIsValid_ReturnsOkResultWithObject()
        {
            //arrange
            var partchDoc = new JsonPatchDocument<ApplicantEditBindModel>();

            //act
            var actionResult = applicantsController.Patch(Guid.Parse(Id), partchDoc);

            //assert
            applicantsController
                .ModelState
                .IsValid
                .Should()
                .BeTrue();

            actionResult
                .Should()
                .BeAssignableTo<OkObjectResult>();
        }
    }
}
